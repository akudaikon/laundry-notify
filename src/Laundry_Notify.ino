#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServer.h>
#include <WiFiManager.h>
#include <DNSServer.h>
#include <WiFiClient.h>
#include <PubSubClient.h>
#include <HLW8012.h>

#include "mqtt_config.h"

#define RELAY_PIN                       12
#define LED_PIN                         15
#define BUTTON_PIN                      0
#define SEL_PIN                         5
#define CF1_PIN                         13
#define CF_PIN                          14

// HLW8012 circuit parameters
#define CURRENT_RESISTOR                0.001
#define VOLTAGE_RESISTOR_UPSTREAM       (5 * 470000)
#define VOLTAGE_RESISTOR_DOWNSTREAM     1000

#ifdef WASHER
  #define DEVICE_NAME                   "Washer"
  #define DEVICE_ICON                   "mdi:washing-machine"
  #define MQTT_AVAIL_TOPIC              "laundry/washer/availability"
  #define MQTT_STATE_TOPIC              "laundry/washer/state"
  #pragma message("Building Washer")
#elif defined DRYER
  #define DEVICE_NAME                   "Dryer"
  #define DEVICE_ICON                   "mdi:tumble-dryer"
  #define MQTT_AVAIL_TOPIC              "laundry/dryer/availability"
  #define MQTT_STATE_TOPIC              "laundry/dryer/state"
  #pragma message("Building Dryer")
#endif

#define ON                              true
#define OFF                             false

#ifdef WASHER
  #define ON_CURRENT_MIN                0.06
#elif defined DRYER
  #define ON_CURRENT_MIN                2.00
#endif
#define ON_STATE_DELAY                  3   // x UPDATE_RATE
#define OFF_STATE_DELAY                 6   // x UPDATE_RATE

#define WAIT_BLINK_RATE                 250
#define RUNNING_BLINK_DELAY             1000
#define HLW8012_TOGGLE_RATE             2500
#define UPDATE_RATE                     5000
#define START_TIMEOUT                   60000

enum { IDLE, START_WAIT, RUNNING };
uint8_t state = IDLE;

uint32_t lastTimeout = 0;
uint32_t lastBlink = 0;
uint32_t lastToggle = 0;
uint32_t lastUpdate = 0;

bool machineState = OFF;
const char* stateNames[2] = { "Idle", "Running" };
uint8_t stateChangeCount = 0;

WiFiManager wifiManager;
MDNSResponder mdns;
ESP8266WebServer server(80);
ESP8266HTTPUpdateServer httpUpdater;

WiFiClient mqttClient;
PubSubClient mqtt(MQTT_HOST, MQTT_PORT, mqttClient);

HLW8012 hlw8012;

void MQTT_haDiscovery()
{
  String id;
  String dev;
  String topic;

  char buf[7];
  uint8_t macAddr[6];
  WiFi.macAddress(macAddr);
  sprintf(buf, "%02x%02x%02x", macAddr[3], macAddr[4], macAddr[5]);
  id = buf;

  dev = "\"dev\": { \"ids\": \"" + id + "\", \"name\": \"" + DEVICE_NAME + "\", \"mdl\": \"" + DEVICE_NAME + "\", \"mf\": \"Akudaikon\" }";

  String config;

  topic = "homeassistant/sensor/laundry_" + id + "/config";
  
  config = "{\"name\": \"" DEVICE_NAME "\", \"obj_id\": \"" DEVICE_NAME "\", \"icon\": \"" DEVICE_ICON "\", \"stat_t\": \"" MQTT_STATE_TOPIC "\", \"avty_t\": \"" MQTT_AVAIL_TOPIC "\", \"uniq_id\": \"" + id + "\", " + dev + " }";
  mqtt.publish(topic.c_str(), config.c_str(), true);
}

void setStateAndMQTT(bool state)
{
  machineState = state;
  mqtt.publish(MQTT_STATE_TOPIC, stateNames[machineState], true);
}

void mqttReconnect()
{
  static long lastReconnect = 0;

  if (millis() - lastReconnect > 5000)
  {
    mqtt.setBufferSize(512);

    String availTopic;

    if (mqtt.connect(DEVICE_NAME, MQTT_USER, MQTT_PASS, MQTT_AVAIL_TOPIC, 0, true, "offline"))
    {
      mqtt.publish(MQTT_AVAIL_TOPIC, "online", true);
      
      MQTT_haDiscovery();
      setStateAndMQTT(OFF);

      lastReconnect = 0;
    }
    else lastReconnect = millis();
  }
}

void sendPower()
{
  String response;
  response = stateNames[machineState];
  response += ",";
  response += hlw8012.getActivePower();
  response += ",";
  response += hlw8012.getCurrent();
  response += ",";
  response += hlw8012.getVoltage();

  server.sendHeader("Cache-Control", "no-cache");
  server.send(200, "text/html", response);
}

void setup()
{
  Serial.begin(115200);

  // Close relay to load
  pinMode(RELAY_PIN, OUTPUT);
  digitalWrite(RELAY_PIN, HIGH);

  // Turn off LED
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);

  // Connect to Wifi
  wifiManager.setConfigPortalTimeout(180);
  if (!wifiManager.autoConnect(DEVICE_NAME)) ESP.reset();

  // Start servers
  mdns.begin(DEVICE_NAME);
  server.on("/", sendPower);
  server.on("/off", [] { digitalWrite(RELAY_PIN, LOW); server.send(200, "text/html", "Off"); });
  server.on("/on", [] { digitalWrite(RELAY_PIN, HIGH); server.send(200, "text/html", "On"); });
  httpUpdater.setup(&server);
  server.begin();

  // Setup MQTT
  mqtt.setBufferSize(512);
  mqtt.setServer(MQTT_HOST, MQTT_PORT);
  mqttReconnect();

  // Init HLW8012 and enable interrupts
  hlw8012.begin(CF_PIN, CF1_PIN, SEL_PIN, HIGH, false);
  hlw8012.setResistors(CURRENT_RESISTOR, VOLTAGE_RESISTOR_UPSTREAM, VOLTAGE_RESISTOR_DOWNSTREAM);
}

void loop()
{
  // Handle MQTT
  if (!mqtt.connected()) mqttReconnect();
  if (mqtt.connected()) mqtt.loop();

  // Handle HTTP server requests
  server.handleClient();

  if (millis() - lastToggle > UPDATE_RATE)
  {
    hlw8012.toggleMode();
    lastToggle = millis();
  }

  // Do logic!
  switch(state)
  {
    case IDLE:
      digitalWrite(LED_PIN, LOW);

      if (millis() - lastUpdate > UPDATE_RATE)
      {
        hlw8012.getActivePower();
        if (hlw8012.getCurrent() > ON_CURRENT_MIN)
        {
          if (++stateChangeCount > ON_STATE_DELAY)
          {
            setStateAndMQTT(ON);
            stateChangeCount = 0;
            state = RUNNING;
          }
        }
        else stateChangeCount = 0;;

        lastUpdate = millis();
      }
    break;

    case START_WAIT:
      if (millis() - lastBlink > WAIT_BLINK_RATE)
      {
        digitalWrite(LED_PIN, !digitalRead(LED_PIN));
        lastBlink = millis();
      }

      if (millis() - lastTimeout > START_TIMEOUT)
      {
        state = IDLE;
      }

      if (millis() - lastUpdate > UPDATE_RATE)
      {
        hlw8012.getActivePower();
        if (hlw8012.getCurrent() > ON_CURRENT_MIN)
        {
          if (++stateChangeCount > ON_STATE_DELAY)
          {
            setStateAndMQTT(ON);
            stateChangeCount = 0;
            state = RUNNING;
          }
        }
        else stateChangeCount = 0;

        lastUpdate = millis();
      }
    break;

    case RUNNING:
      if (millis() - lastBlink > RUNNING_BLINK_DELAY)
      {
        digitalWrite(LED_PIN, !digitalRead(LED_PIN));
        lastBlink = millis();
      }

      if (millis() - lastUpdate > UPDATE_RATE)
      {
        hlw8012.getActivePower();
        if (hlw8012.getCurrent() <= ON_CURRENT_MIN)
        {
          if (++stateChangeCount > OFF_STATE_DELAY)
          {
            setStateAndMQTT(OFF);
            digitalWrite(LED_PIN, LOW);
            stateChangeCount = 0;
            state = IDLE;
          }
        }
        else stateChangeCount = 0;

        lastUpdate = millis();
      }
    break;
  }
}
